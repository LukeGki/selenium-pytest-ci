# Selenium Pytest CI

## Run

Run Pipeline manually

## Test set

* tests-smoke
  * smoke-desktop-chrome-1920x1080
* tests-sanity
  * sanity-desktop-chrome-1920x1080
* tests-regression
  * regression-home-desktop-chrome-1920x1080
  * regression-category-desktop-chrome-1920x1080