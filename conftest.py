import allure
import pytest
from allure_commons.types import AttachmentType

from selenium import webdriver

import os


def pytest_addoption(parser):
    parser.addoption("--resolution")


@pytest.fixture
def resolution(request):
    return request.config.getoption("--resolution")


@pytest.fixture(autouse=True)
def driver(resolution):
    options = webdriver.ChromeOptions()

    if os.name == 'nt':
        driver = webdriver.Chrome(executable_path=r"drivers\chromedriver.exe")
    else:
        options.add_argument("--lang=en-US")
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        driver = webdriver.Chrome(options=options)

    driver.implicitly_wait(5)

    if resolution:
        x_position = resolution.rfind("x")
        display_width = int(resolution[:x_position])
        display_height = int(resolution[x_position + 1:])
        driver.set_window_size(display_width, display_height)

    yield driver

    allure.attach(driver.get_screenshot_as_png(), name="Screenshot", attachment_type=AttachmentType.PNG)
    driver.quit()
