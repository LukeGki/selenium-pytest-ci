import pytest

from pages.base_page import BasePage
from pages.home_page import HomePage


class TestBasePage:
    @pytest.mark.smoke
    @pytest.mark.desktop
    @pytest.mark.mobile
    def test_redirect_to_home_page(self, driver):
        page = BasePage(driver)
        page.load()
        page = HomePage(driver)
        assert page.get_url() == page.url
