import pytest

from pages.category_page import CategoryPage

from data.data_loader import load_test_data


class TestCategoryPage:
    test_data = load_test_data()
    test_data_categories_with_products = [(item["Category number"], item["Product name"], item["Product number"]) for
                                          item in test_data["Categories with products"]]
    test_data_categories_numbers = [(item["Category number"]) for item in test_data["Categories"]]

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_map_marker_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_map_marker() == page.contact_map_marker


    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_phone_number_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_phone_number() == page.contact_phone_number

    @pytest.mark.regression
    @pytest.mark.desktop
    @pytest.mark.parametrize("category_number", test_data_categories_numbers[:2])
    def test_read_contact_email_desktop(self, driver, category_number):
        page = CategoryPage(driver)
        page.load(category_number)
        assert page.get_contact_email() == page.contact_email
