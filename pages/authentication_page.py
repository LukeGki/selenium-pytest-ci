from pages.base_page import BasePage


class AuthenticationPage(BasePage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)

        self.url = self.base_url + "/index.php?controller=authentication"
