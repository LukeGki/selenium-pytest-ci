from pages.base_page import BasePage

from data.data_loader import load_test_data


class HomePage(BasePage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)

        self.url = self.base_url + "/index.php"
        test_data = load_test_data()
        self.title = test_data["Home Page title"]
