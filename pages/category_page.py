from pages.base_page import BasePage


class CategoryPage(BasePage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)

        self.url = self.base_url + "/index.php?id_category={}&controller=category"
        self.product_selector = "ul[class*='product_list'] a[class='product_img_link'][title='{}']"
        self.add_product_to_cart_selector = "a[title='Add to cart'][data-id-product='{}']"

    def load(self, number):
        self.driver.get(self.url.format(number))
        return self
