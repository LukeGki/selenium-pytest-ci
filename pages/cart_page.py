from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class CartPage(BasePage):
    def __init__(self, driver):
        BasePage.__init__(self, driver)

        self.url = self.base_url + "/index.php?controller=order"
        self.empty_cart_alert_selector = "div#center_column p[class='alert alert-warning']:not([style='display:none'])"
        self.proceed_to_checkout_button_selector = "p[class*='cart_navigation'] a[title='Proceed to checkout']"
        self.delete_product_button_selector = "//table[@id='cart_summary']//p[@class='product-name']/a[text()='{" \
                                              "}']/../../..//a[@title='Delete'] "

    def is_cart_empty(self):
        return self.driver.find_elements(By.CSS_SELECTOR, self.empty_cart_alert_selector)

    def click_proceed_to_checkout_button(self):
        element = self.driver.find_element(By.CSS_SELECTOR, self.proceed_to_checkout_button_selector)
        element.click()
        return self

    def click_delete_product(self, product):
        element = self.driver.find_element(By.XPATH, self.delete_product_button_selector.format(product))
        element.click()
        return self
