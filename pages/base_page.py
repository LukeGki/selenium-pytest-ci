from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from data.data_loader import load_test_data


class BasePage:
    def __init__(self, driver):
        self.driver = driver

        test_data = load_test_data()
        self.base_url = test_data["URL"]
        self.url = self.base_url
        self.contact_map_marker = test_data["Contact map marker"]
        self.contact_phone_number = test_data["Contact phone number"]
        self.contact_email = test_data["Contact email"]

        self.categories_menu_selector = "div#block_top_menu"
        self.category_selector = "div#block_top_menu ul[class*='menu-content']>li>a[title='{}']"
        self.product_selector = "ul#homefeatured a[class='product_img_link'][title='{}']"
        self.add_product_to_cart_selector = "ul#homefeatured a[data-id-product='{}']"
        self.product_added_to_cart_selector = "div#layer_cart[style*='display: block']"
        self.product_more_selector = "//ul[contains(@class,'product_list')]//a[@class='product_img_link'][@title='{" \
                                     "}']/../../..//a[@title='View'] "
        self.product_in_cart_selector = "div[class='shopping_cart'] a[class='cart_block_product_name'][title='{}']"
        self.cart_link_locator = "a[title='View my shopping cart']"
        self.sign_in_link_locator = (By.CSS_SELECTOR, "a[class='login']")
        self.store_information_menu_selector = "section#block_contact_infos"
        self.contact_map_marker_locator = (By.XPATH, "//i[@class='icon-map-marker']/..")
        self.contact_phone_number_locator = (By.CSS_SELECTOR, "i[class='icon-phone']+span")
        self.contact_email_locator = (By.CSS_SELECTOR, "i[class='icon-envelope-alt']+span")

    def load(self, *args):
        self.driver.get(self.url)
        return self

    def get_url(self):
        return self.driver.current_url

    def get_title(self):
        return self.driver.title

    def click_categories_menu(self):
        element = self.driver.find_element(By.CSS_SELECTOR, self.categories_menu_selector)
        element.click()
        return self

    def click_category(self, category):
        element = self.driver.find_element(By.CSS_SELECTOR, self.category_selector.format(category))
        element.click()
        return self

    def add_product_to_cart(self, product, product_number):
        element = self.driver.find_element(By.CSS_SELECTOR, self.product_selector.format(product))
        self.driver.execute_script("arguments[0].scrollIntoView(true);", element)
        ActionChains(self.driver).move_to_element(element).perform()
        element = self.driver.find_element(By.CSS_SELECTOR, self.add_product_to_cart_selector.format(product_number))
        element.click()
        self.driver.find_element(By.CSS_SELECTOR, self.product_added_to_cart_selector)
        return self

    def click_product_more(self, product):
        element = self.driver.find_element(By.CSS_SELECTOR, self.product_selector.format(product))
        self.driver.execute_script("arguments[0].scrollIntoView(true);", element)
        ActionChains(self.driver).move_to_element(element).perform()
        element = self.driver.find_element(By.XPATH, self.product_more_selector.format(product))
        element.click()
        return self

    def is_product_in_cart(self, product):
        return self.driver.find_elements(By.CSS_SELECTOR, self.product_in_cart_selector.format(product))

    def click_cart_link(self):
        element = self.driver.find_element(By.CSS_SELECTOR, self.cart_link_locator)
        element.click()
        return self

    def click_sign_in_link(self):
        element = self.driver.find_element(*self.sign_in_link_locator)
        element.click()
        return self

    def click_store_information_menu(self):
        element = self.driver.find_element(By.CSS_SELECTOR, self.store_information_menu_selector)
        element.click()
        return self

    def get_contact_map_marker(self):
        element = self.driver.find_element(*self.contact_map_marker_locator)
        return element.get_attribute('innerText')

    def get_contact_phone_number(self):
        element = self.driver.find_element(*self.contact_phone_number_locator)
        return element.get_attribute('innerText')

    def get_contact_email(self):
        element = self.driver.find_element(*self.contact_email_locator)
        return element.get_attribute('innerText')
