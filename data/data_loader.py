import json
import os


def load_test_data():
    return json.loads(open(os.path.join(os.path.dirname(__file__), "test_data.json"), "r").read())
